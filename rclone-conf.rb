#!/usr/bin/env ruby

require 'runcom'
require 'homecoming'
require 'yaml'
require 'optparse'

VERSION = "0.0.1"

class RcloneConf

  $CFGNAME = "rclone.yml"

  attr_accessor :cfg

  def initialize(initialCfg = {}, baseDir = nil)
    @baseDir ||= (initialCfg["dir"] || Dir.pwd)

    globalCfg =
      Runcom::Config.new "rclone", file_name: 'rclone-conf.yml'

    found = Homecoming.find($CFGNAME, @baseDir)
    raise "No conf file found (#{$CFGNAME})" if found.length < 1

    if found.length > 0
      @baseDir = File.dirname(found.last)
    end

    @cfg = found.reduce(globalCfg.to_h.merge initialCfg) do |cfg, f|
      data = YAML::load(File.new(f))
      raise "Bad config file #{f}" unless data
      cfg.merge data
    end

    set_rclone_pass
  end

  def set_rclone_pass
    if @cfg["pass_cmd"]
      pass = `#{@cfg["pass_cmd"]}`
      ENV["RCLONE_CONFIG_PASS"] = pass.chomp
    end
  end

  def global_args
    args = []
    args << "--dry-run" if @cfg["dry_run"]
    args << "-P"
    args
  end

  def filters
    fs = @cfg["filters"] || []
    fs.reduce([]) do |acc, f|
      if f.instance_of? String
        f = "+ #{f}"
      elsif f["ex"]
        f = "- #{f["ex"]}"
      elsif f["in"]
        f = "+ #{f["in"]}"
      end
      acc + ["--filter", f]
    end
  end

  def exec(cmd, args = [])
    args = ["rclone"] + global_args +
           filters + [cmd] + args
    puts args.join(" ") if verbose
    system(*args)
  end

  def verbose
    @cfg['verbose']
  end

  def sync(from, to)
    exec("copy", [
           File.join(from, @cfg["subdir"] || ""),
           File.join(to, @cfg["subdir"] || "")])
  end

  def ls
    exec("ls", [remote])
  end

  def check
    oneway = []
    if @cfg['oneway'] then oneway << "--one-way" end
    exec("check", oneway + [local, remote])
  end

  def push
    sync(local, remote)
  end

  def pull
    sync(remote, local)
  end

  def remote
    @cfg['remote'] || raise("No remote defined in config")
  end

  def local
    @cfg['local'] || @baseDir
  end
end

options = {}

OptionParser.new do |opts|
  opts.banner = "Usage: example.rb [options]"

  opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
    options["verbose"] = v
  end
  opts.on("-n", "--[no-]dry-run", "Dry run") do |d|
    options["dry_run"] = d
  end
  opts.on("-d:", "--dir:", "Set directory to start from") do |d|
    options["dir"] = d
  end
  opts.on("-s:", "--subdir:", "Sub directory with the remote/local") do |s|
    options["subdir"] = s
  end
end.parse!

r = RcloneConf.new(options)

cmd = ARGV[0] || raise("no command")

case cmd
when "push"
  r.push
when "pull"
  r.pull
when "about"
  puts "rclone-conf v#{VERSION}"
  puts "Config:"
  puts YAML.dump(r.cfg)
when "ls"
  r.ls
when "check"
  r.check
else
  puts "Unknown command: #{cmd}"
end
